import unittest
from task import generate_game, get_score


class WrongDataCases(unittest.TestCase):
    def test_negative(self):
        game_stamps = generate_game()
        self.assertEqual(get_score(game_stamps, -111), (game_stamps[0]['score']['home'], game_stamps[0]['score']['away']))

    def test_negative_zero(self):
        game_stamps = generate_game()
        self.assertEqual(get_score(game_stamps, -0), (game_stamps[0]['score']['home'], game_stamps[0]['score']['away']))

    def test_zero(self):
        game_stamps = generate_game()
        self.assertEqual(get_score(game_stamps, 0), (game_stamps[0]['score']['home'], game_stamps[0]['score']['away']))

    def test_max(self):
        game_stamps = generate_game()
        self.assertEqual(get_score(game_stamps, game_stamps[-1]['offset']), (game_stamps[-1]['score']['home'], game_stamps[-1]['score']['away']))

    def test_out_of_max(self):
        game_stamps = generate_game()
        self.assertEqual(get_score(game_stamps, 9999999999999999999), (game_stamps[-1]['score']['home'], game_stamps[-1]['score']['away']))

class UsualDataCases(unittest.TestCase):
    def test_equal(self):
        game_stamps = generate_game()
        offset = game_stamps[33333]['offset']
        self.assertEqual(get_score(game_stamps, offset), (game_stamps[33333]['score']['home'],game_stamps[33333]['score']['away']))

    def test_lt_offset_plus_one(self):
        game_stamps = generate_game()
        offset = game_stamps[33333]['offset']
        game_stamps[33332]['offset'] -= 2
        game_stamps[33333]['offset'] -= 1
        self.assertEqual(get_score(game_stamps, offset), (game_stamps[33333]['score']['home'],game_stamps[33333]['score']['away']))


if __name__ == '__main__':
    unittest.main()