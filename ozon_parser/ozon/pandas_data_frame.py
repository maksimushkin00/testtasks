import pandas
import json

with open('os_100.json') as f:
    data = json.load(f)
    res = pandas.DataFrame(data).value_counts()
    res.columns = ['OS', 'Times appeares']
    print(res)
    with open('res.txt', 'a') as f:
        res_to_write = res.to_string(header=False)
        f.write(res_to_write)
