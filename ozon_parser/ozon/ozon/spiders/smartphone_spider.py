# 
# Sleeps and other slowest beautiful stuff
# Can be optimized by WebDriverWait, but it takes a lot of time to debug. Sleeps are more reliable at lower development cost

import scrapy
from scrapy import Selector
from seleniumbase import Driver # can bypass CloudFlare
from selenium.webdriver.common.by import By
from time import sleep
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from scrapy.crawler import CrawlerProcess
import pandas
import json


class SmartphoneSpiderSpider(scrapy.Spider):
    name = 'smartphone_spider'
    allowed_domains = ["toscrape.com"]
    start_urls = ['http://quotes.toscrape.com']

    # Using simple a website to not get banned with scrapy
    def start_requests(self):
        for url in self.start_urls:
            yield scrapy.Request(url=url, callback=self.parse_smartphones_page)

    def parse_smartphones_page(self, response):
        # get the driver with cloudflare bypass and change location to Moscow
        driver = Driver(uc=True, headless=True, headless2=True)
        driver.get('https://www.ozon.ru/category/smartfony-15502/?sorting=rating')
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'q0'))).click()
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 't9d'))).click()
        sleep(1)
        driver.find_element(By.CLASS_NAME, 'd3t').click()
        sleep(1)
        driver.refresh()

        # crawl pages until done        
        counter = 0
        page = 'https://www.ozon.ru/category/smartfony-15502/?sorting=rating'
        while counter < 100:
            sleep(3)
            driver.implicitly_wait(10)
            selenium_response_text = driver.page_source
            new_selector = Selector(text=selenium_response_text)
            link_l = new_selector.css('a.tile-hover-target.k8n::attr(href)').getall() # get all links on page

            for link in link_l:
                new_url = 'https://www.ozon.ru' + link
                driver.get(new_url)
                try: # waiting for page unless it got loaded
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH,
                    '//*[@id="section-characteristics"]/div[1]/div/div/div/div/button')))
                except Exception:
                    driver.refresh()
                    sleep(3)


                # Refresh if some info didn't show up
                os_name, os_full_name = False, False
                tries = 0
                while True: # do while like stuff
                    tries += 1
                    response_text = driver.page_source
                    selector = Selector(text=response_text)
                    os_name = selector.xpath("//*[contains(text(), 'Операционная система')]/../../dd/a/text()").getall()
                    os_full_name = selector.xpath(f"//*[contains(text(), 'Версия')]/../../dd/a/text()").getall()
                    if not os_full_name:
                        os_full_name = selector.xpath(f"//*[contains(text(), 'Версия')]/../../dd/text()").getall()

                    if (not os_full_name) and (not os_name): 
                        driver.refresh()
                        sleep(3)
                    elif tries == 5: # God bless not given info
                        os_full_name = 'Not given'
                        break
                    else:
                        break

                # OS version not found
                if not os_full_name:
                    os_full_name = os_name

                counter += 1
                # print(f'####### counter: {counter} os: {os_name} version: {os_full_name}')
                yield {'OS': os_full_name[0],}
                if counter == 100:
                    break
            
            next_page = new_selector.css('a._4-a1::attr(href)')
            next_page = 'https://www.ozon.ru' + next_page.get()
            driver.get(page)
            page = next_page
            driver.get(next_page)


# Start the spider and get pandas dataframe
if __name__ == "__main__": 
    process = CrawlerProcess({
        'FEED_URI': 'os_100.json',
        'FEED_FORMAT': 'json',
    })
    process.crawl(SmartphoneSpiderSpider)
    process.start()
    with open('os_100.json') as f:
        data = json.load(f)
    res = pandas.DataFrame(data).value_counts()
    with open('res.txt', 'a') as f:
        res_to_write = res.to_string(header=False)
        f.write(res_to_write)
